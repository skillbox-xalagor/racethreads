#include <array>
#include <windows.h>
#include <iostream>
#include <map>
#include <string>
#include <thread>
#include <condition_variable>
#include <random>

const int TRACKLINES = 2;
const int TRACKLENGTH = 5;

bool IsGameEnded = false;
bool IsNotified = false;

struct RaceElement
{
	bool obstacle = false;
	char symbol = ' ';
};

const RaceElement empty = { false, ' ' };
const RaceElement wall = { true, '|' };
const RaceElement kart = { true, '@' };
const RaceElement obstacle = { true, '#' };

std::array<std::array<RaceElement, TRACKLINES>, TRACKLENGTH> RaceGrid;

HANDLE hConsole = GetStdHandle(STD_OUTPUT_HANDLE);

std::condition_variable CV_MoveRoad;

int KartPosition = 0;

void DrawRaceTrack(std::mutex& drawLocker, std::mutex& gridAccessLocker)
{
	std::unique_lock<std::mutex> gridAccessUL(gridAccessLocker, std::defer_lock);
	while(!IsGameEnded) {
		std::unique_lock<std::mutex> drawUL(drawLocker);
		while (!IsNotified)
		{
			CV_MoveRoad.wait(drawUL);
		}
		system("cls");
		for (int i = 0; i < TRACKLENGTH; i++)
		{
			std::cout << wall.symbol;
			for (int j = 0; j < TRACKLINES; j++)
			{
				if ((KartPosition == j) && (i == TRACKLENGTH - 1))
				{
					std::cout << kart.symbol;
				}
				else
				{
					gridAccessUL.lock();
					std::cout << RaceGrid[i][j].symbol;
					gridAccessUL.unlock();
				}
			}
			std::cout << wall.symbol;
			std::cout << std::endl;
		}
		IsNotified = false;
	}
}

void GenerateNewRoadLine(std::array<RaceElement, TRACKLINES>& line);

void MoveKart(std::mutex& drawLocker)
{
	while(!IsGameEnded)
	{
		if ((GetKeyState(VK_LEFT) & 0x8000) && KartPosition > 0)
		{
			KartPosition--;
			{
				std::unique_lock<std::mutex> drawUL(drawLocker);
				IsNotified = true;
				CV_MoveRoad.notify_one();
			}
		}
		if ((GetKeyState(VK_RIGHT) & 0x8000) && KartPosition < (TRACKLINES - 1))
		{
			KartPosition++;
			{
				std::unique_lock<std::mutex> drawUL(drawLocker);
				IsNotified = true;
				CV_MoveRoad.notify_one();
			}
		}
	}
}

void MoveRoad(std::mutex& drawLocker, std::mutex& gridAccessLocker)
{
	auto timer = std::chrono::milliseconds(1000);
	auto timerDelta = std::chrono::milliseconds(10);
	while (!IsGameEnded)
	{
		std::this_thread::sleep_for(timer);
		timer -= timerDelta;
		for (int i = TRACKLENGTH - 1; i >= 1; i--)
		{
			RaceGrid[i] = RaceGrid[i - 1];
		}
		GenerateNewRoadLine(RaceGrid[0]);
		{
			std::unique_lock<std::mutex> lock(drawLocker);
			IsNotified = true;
			CV_MoveRoad.notify_one();
		}
	}
}

const double chanceDefault = 0.01;
double chance = 1;
const double chanceMultiplier = 2;
void GenerateNewRoadLine(std::array<RaceElement, TRACKLINES>& line)
{
    std::random_device rd;
    std::mt19937 mt(rd());
    std::bernoulli_distribution dist(chance);
	bool IsObstacleSet = false;
	for (auto& i : line)
	{
		if (dist(mt) && !IsObstacleSet)
		{
			i = obstacle;
			IsObstacleSet = true;
			chance = chanceDefault;
		}
		else
		{
			i = empty;
			chance = min(1, chance * chanceMultiplier);
		}
	}
}

void GameModeTick(std::mutex& drawLocker)
{
	while (true)
	{
		std::unique_lock<std::mutex> drawUL(drawLocker);
		if (RaceGrid[TRACKLENGTH - 1][KartPosition].obstacle)
		{
			IsGameEnded = true;
		}
	}
}

int main()
{
	for (auto& i : RaceGrid)
	{
		for (auto& j : i)
		{
			j = empty;
		}
	}

	std::mutex drawMutex;
	std::mutex gridAccessMutex;

	std::thread MoveRoadThread = std::thread([&](){ MoveRoad(drawMutex, gridAccessMutex); });
	MoveRoadThread.detach();

	std::thread MoveKartThread = std::thread([&](){ MoveKart(drawMutex); });
	MoveKartThread.detach();

	std::thread GameModeThread = std::thread([&](){ GameModeTick(gridAccessMutex); });
	GameModeThread.detach();

	std::thread DrawRaceTrackThread = std::thread([&](){ DrawRaceTrack(drawMutex, gridAccessMutex); });
	DrawRaceTrackThread.join();

	system("cls");
	std::cout << "Game ended" << std::endl;

	system("pause");
	return 0;
}